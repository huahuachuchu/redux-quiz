import React from 'react';
import './App.less';
import Header from './Header';
import Home from "./home/Home";
import {Route, Switch} from "react-router-dom";
import {BrowserRouter as Router} from "react-router-dom";
import NoteDetails from './reducers/pages/CreatNote';
import CreatNote from './reducers/pages/CreatNote';


class App extends React.Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Header />
            <Route path='/' component={Home} />
            <Route path='/note' component={NoteDetails} />
            <Route path='/note' component={CreatNote} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;