import React from 'react';
import {MdAddToPhotos} from 'react-icons/md';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import NoteDetails from "../reducers/pages/CreatNote";
import CreatNote from '../reducers/pages/CreatNote';

class Home extends React.Component {
  render() {
    return (
      <div>
          <ul>
            <li>
              <Link to='/note'>HTML5</Link>
            </li>
            <li>
              <Link to='/note'>CSS预处理语言</Link>
            </li>
            <li>
              <Link to='/note'>HTML5</Link>
            </li>
            <li>
              <Link to='/note'>CSS预处理语言</Link>
            </li>
            <li>
              <Link to='/note'>HTML5</Link>
            </li>
            <li>
              <Link to='/note'>
                <MdAddToPhotos className="add-icon"/>
              </Link>
            </li>
          </ul>
      </div>
    );
  }
}

export default Home;