import React from "react";
import {MdEventNote} from 'react-icons/md';
import './Header.less';


class Header extends React.Component {
  render() {
    return (
      <div>
        <header>
          <MdEventNote className="note-icon"/>
          <p>NOTES</p>
        </header>
      </div>
    );
  }
}

export default Header;