import React from 'react';
import Header from "../../Header";

const Menu = ({id,title,description}) => {
  return (<div>
    <Header />
    <div>
      <label>Id: </label>
      <span>{id}</span>
    </div>
    <div>
      <label>Title: </label>
      <span>{title}</span>
    </div>
    <div>
      <label>Description: </label>
      <span>{description}</span>
    </div>
  </div>)
};

export default Menu;