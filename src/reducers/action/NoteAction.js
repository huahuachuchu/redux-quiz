export const SET_NENU = 'SET_MENU';
export const GET_MENU = 'GET_MENU';

function setMenu(id,title,description) {
  return{
    type:SET_NENU,
    payload:{}
  }
}
function getMenu(){
  return async (dispatch) => {
    let msg = await fetch('http://localhost:8080/api/posts',{
      method: 'GET',
      // body: JSON.stringify({"aa":"aa"}),
      header: {'content-type':'application/json; charset=utf-8'}
    }).then((res)=>res.json());
    const state = {nodes: msg};
    dispatch({
      type: GET_MENU,
      payload: state
    });
  };
}